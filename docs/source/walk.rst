=======================================
 :mod:`xotless.walk` -- Walking graphs
=======================================

.. module:: xotless.walk

.. autofunction:: pruned_prefix_walk

.. autoclass:: NodeRef
