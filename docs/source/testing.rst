==============================================
 `xotless.testing`:mod: -- Testing facilities
==============================================

.. module:: `xotless.testing`

.. warning:: This module is still experimental, and it can change its API (or
             be removed completely) without deprecations.


`xotless.testing.strategies.domains`:mod:
=========================================

.. automodule:: xotless.testing.strategies.domains
   :members: float_ranges, date_ranges, members_of_float_ranges,
             members_of_date_ranges, many_float_ranges, many_date_ranges,
             float_intervals, date_intervals, get_float_member_strategy,
             get_date_member_strategy
