=======================================================
 :mod:`xotless.tracing` -- Integration with Sentry SDK
=======================================================

.. module:: xotless.tracing

.. testsetup::

   from xotless.tracing import *

.. autofunction:: get_module_sentry_spanner
