=============================================================
 :mod:`xotless.pickablenv` -- Make Odoo environment pickable
=============================================================

.. module:: xotless.pickablenv

.. warning:: This module depends on Odoo, but this package doesn't require it.
   Also, this module is experimental and may be removed in future releases.

.. deprecated:: 1.8.0

   This module has been deprecated because restoration of the api.Environment
   is not a trivial task and is full of details and issues causing bugs.

.. autoclass:: EnvironmentData

.. autoclass:: PickableRecordset

.. autofunction:: FORCED_ENVIRONMENT

.. autofunction:: CURRENT_ENVIRONMENT
